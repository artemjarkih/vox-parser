<?php

function get_data_log($url)
{
	$content = file_get_contents($url);

	preg_match('~(\{"COMMAND":"AddCallHistory.+?(...|\}))\n~is', $content, $preg);

	if (empty($preg[1])) {
		return false;
	}

	$json = $preg[1];

	if ($preg[2] == '...') {
		$json = substr($json, -strlen($json), strrpos($json, ',')).'}';
	}

	return json_decode($json, true);
}

// Функция склонения слов после числительных
function plural_form($number, $after)
{
	$cases = [2, 0, 1, 1, 1, 2];

	return $number.' '.$after[
		($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[ min( $number % 10, 5) ]
	];
}

function time_left($num)
{
	if ($num < 1)
		return '0 секунд';

	$day_fl = floor($num / 86400); $num -= (86400 * $day_fl);
	$hour_fl = floor($num / 3600); $num -= (3600 * $hour_fl);
	$minute_fl = floor($num / 60); $num -= (60 * $minute_fl);
	$second_fl = floor($num);
	$result = '';

	if ($day_fl > 0) {
		$result .= ($day_fl > 0 ? plural_form($day_fl, ['день','дня','дней']) : '');
		$result .= ($hour_fl > 0 ? ' '.plural_form($hour_fl, ['час','часа','часов']) : '');
	}
	elseif ($day_fl == 0 && $hour_fl > 0) {
		$result .= ($hour_fl > 0 ? plural_form($hour_fl, ['час','часа','часов']):'');
		$result .= ($minute_fl > 0 ? ' '.plural_form($minute_fl, ['минуту','минуты','минут']):'');
	} else {
		$result .= ($minute_fl > 0 ? plural_form($minute_fl, ['минуту','минуты','минут']):'');
		$result .= ($second_fl > 0 ? ' '.plural_form($second_fl, ['секунду','секунды','секунд']):'');
	}

	return $result;
}

// Показ даты в удобном формате
function data_ru($num)
{
	if (date('Y', $num) == date('Y'))
		$result_init = date('d M H:i', $num);
	else
		$result_init = date('d M Y в H:i', $num);

	$str_is = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	$str_res = ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'];

	return str_replace($str_is, $str_res, $result_init);
}

if (! empty($_GET['url'])) {
	$info = get_data_log($_GET['url']);
}

$title = 'Лог Voximplant, alpha v.0.2 ';

//Строка пустая?
foreach ($get_field as $key => $values)
        {
           $line .= "<tr>\n";
                $line .= "\t<td>" . $key . "</td>\n" ;
                foreach ($values as $cell)
                {
                    if($cell > 0 ){
                        echo $line."\t<td>" . $cell . "</td>\n</tr>\n";
                    }
                }
            }
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $title ?></title>
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,400,300,600&subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic" rel="stylesheet" type="text/css">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/site.webmanifest">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#30aacc">
	<meta name="msapplication-TileColor" content="#1891e2">
	<meta name="theme-color" content="#ffffff">
</head>
<body>
	<style>
		body {
			padding: 10px;
			font-family: 'Open Sans', sans-serif;
			margin: 0 auto;
			max-width: 600px;
			color: #7D9CB8;
			/* background-color: #EDEEF0;*/
			padding: 0 2px;
		}
		h1 {
			margin-bottom: 12px;
			margin-top: 0px;
		}
		textarea, input[type='text'] {
			padding: 3px;
		}
		.block {
			border: 1px solid #E3E4E8;
			margin: 2px 0;
			color: #505559;
			background-color:#FFFFFF;
			padding: 12px;
		}
		.title {
			color: #d62020;
		}
		.c-green {
			color: green;
		}
		.c-orange {
			color: orange;
		}
		.c-blue {
			color: blue;
		}
		.act {
			transform: scale(1.4);
			vertical-align: middle;
		}
		.item {
			padding: 4px;
			margin-bottom:15px;
		}
		.mb-def {
			margin-bottom: 5px;
		}
		.mb-10 {
			margin-bottom: 10px;
		}
		.mb-15 {
			margin-bottom: 15px;
		}
		table.nt {
			border-collapse: collapse
		}
		table.nt td,table.nt th {
			padding: .3em .5em
		}
		table.nt th {
			background: grey;
			color: #fff;
			border: 1px solid #909090
		}
		table.nt td {
			border: 1px solid #c1c1c1
		}
		table.nt tr:nth-child(even) td {
			background: #f7f7f7
		}
		table.nt tr:hover td {
			background: #fff3d7
		}
		table.nt td.bts button {
			display: block;
			width: 100%;
			margin: .3em 0
		}
		table.nt.color tr:nth-child(even) td {
			background: #fafcfd
		}
		table.nt.color tr:hover td {
			background: #fff3d7
		}
		table.nt.color th {
			background: #39a7c1;
			border-color: #2c8aa0
		}
	</style>
	<div class="block">
		<h1><?= $title ?></h1>
		<div class="mb-15">
			<form>
				<div class="mb-10">
					<input type="text" name="url" placeholder="URL">
				</div>
				<div>
					<input type="submit" value="Получить">
				</div>
			</form>
		</div>

		<?php if (isset($info)): ?>

			<?php if (is_array($info)): ?>

				<div class="mb-10">
					<a href="<?= htmlspecialchars($_GET['url']) ?>" target="_blank">Ссылка на лог</a>
				</div>

				<?php if (! empty($info['URL'])): ?>

					<div class="mb-10">
						<audio controls src="<?= $info['URL'] ?>"></audio>
					</div>

				<?php endif; ?>

				<div>
					<h3>Информация по клиенту</h3>
					<table class="nt">
						<tr>
							<td>ID аккаунта клиента:</td>
							<td><?= htmlspecialchars($info['ACCOUNT_ID']) ?></td>
						</tr>
						<tr>
							<td>Логин звонящего номера<br>в настройках на портале:</td>
							<td><?= htmlspecialchars($info['ACCOUNT_SEARCH_ID']) ?></td>
						</tr>
					</table>
				</div>

				<div>
					<h3>Информация по порталу</h3>
					<table class="nt">
						<tr>
							<td>Звонили с:</td>
							<td><?= htmlspecialchars($info['PORTAL_NUMBER']) ?></td>
						</tr>
						<tr>
							<td>Звонили на:</td>
							<td><?= htmlspecialchars($info['PHONE_NUMBER']) ?></td>
						</tr>
						<tr>
							<td>Результат звонка:</td>
							<td><?= ($info['CALL_STATUS'] ? '<span style="color:green">Успешно</span>' : '<span style="color:red">Провал</span>') ?></td>
						</tr>
						<tr>
							<td>Дата звонка:</td>
							<td><?= data_ru( intval($info['CALL_START_DATE'] / 1000) ) ?></td>
						</tr>
						<tr>
							<td>Время диалога:</td>
							<td><?= time_left($info['CALL_DURATION']) ?></td>
						</tr>
						<tr>
							<td>Страна пользователя:</td>
							<td><?= htmlspecialchars($info['USER_COUNTRY']) ?></td>
						</tr>
						<tr>
							<td>Тип подключения номера:</td>
							<td><?= htmlspecialchars($info['PORTAL_TYPE']) ?></td>
						</tr>
						<tr>
							<td>Блокировка:</td>
							<td><?= htmlspecialchars($info['BLOCKED']) ?></td>
						</tr>
						<tr>
							<td>Участие в CRM:</td>
							<td><?= htmlspecialchars($info['CRM']) ?></td>
						</tr>
				  </table>
				</div>

			<?php else: ?>

				<div>Нет данных, ты сделал что-то не так или лог был просрочен</div>

			<?php endif; ?>

		<?php endif; ?>
	</div>
</body>
</html>
